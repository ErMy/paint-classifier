from os import listdir, walk
from os.path import isfile, join
import csv
import numpy as np

IMG_HEIGHT = 48
IMG_WIDTH = 48
CHANNELS = 1
NUM_CATEGORIES = 6


"""
Change label (last number in csv files) of all files in path
"""
def change_label(path, label):
    all_files = [join(path, f) for f in listdir(path) if isfile(join(path, f))]

    for file in all_files:
        with open(file, 'r', newline='') as csv_file:
            data = list(csv.reader(csv_file, delimiter=','))
            # set label
            data[0][-1] = int(label)
        writer = csv.writer(open(file, 'w', newline=''))
        writer.writerow(data[0])


def load_dataset(path="Samples"):
    """
    Load image data and return them as two lists (images, labels)
    """
    filelist = []

    # r=root, d=directories, f= files
    for r, d, f in walk(path):
        for file in f:
            if file.endswith(".csv"):
                filelist.append(join(r, file))

    image_list = []
    label_list = []

    for f in filelist:
        with open(f, 'r') as csv_file:
            data_iter = csv.reader(csv_file, delimiter=',')
            data = list(data_iter)
            image = np.asarray(data[0], dtype=float)
        label = int(image[-1])
        image = image[0:-1]
        image = image / 255.0
        image = image.reshape((48, 48, 1))
        image_list.append(image)
        label_list.append(label)
    return image_list, label_list


def load_image(file_path):
    """
    Returns image/csv file as numpy array
    """
    with open(file_path, 'r') as csv_file:
        data_iter = csv.reader(csv_file, delimiter=',')
        data = [data for data in data_iter]
        image = np.asarray(data, dtype=float)
        image = image / 255.0
        image = image.reshape(IMG_WIDTH, IMG_HEIGHT, CHANNELS)
    return image


if __name__ == "__main__":
    path = join("Samples", "5ribbon")
    change_label(path, 5)


