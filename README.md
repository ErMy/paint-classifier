# Paint Classifier


The goal is to classify symbols drawn by the player in a game as shown in the following video.

![Imgur](https://i.imgur.com/FbxQQX6.mp4)


The agent uses a CNN to classify hand-drawn symbols and a convolutional autoencoder to detect anomalies. The player draws a symbol ingame, which is then saved as a csv file and then read by the python script.
Images are in 48x48x1 format.
First, the CNN classifies the image. There are 4 valid symbols and an extra class for invalid symbols.
If the CNN classifies the symbol as valid (not invalid class and high confidence), then an autoencoder determines if the symbol is too different / if it's an anomaly.
If it's not, the reconstructed image from the autoencoder is classified again.
If the symbol passes all tests, it is valid and the determined class index is sent back to the game.

The project implements two CNNs and two convolutional autoencoders.
Run **agent.py** to plot test images and their predictions. Agent.py searches for saved models at startup and trains new models if no saved models are found.
Run **server.py** together with the game build below to test it. (VR necessary)
Classifier and Autoencoder classes can be run as well to plot their outputs.
