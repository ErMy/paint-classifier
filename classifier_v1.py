import numpy as np
from os.path import join, exists
from tensorflow import keras
from tensorflow.keras.layers import Flatten, Dense, Dropout, Conv2D, MaxPooling2D
from tensorflow.keras import Sequential
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt
from utility import IMG_HEIGHT, IMG_WIDTH, CHANNELS, NUM_CATEGORIES, load_dataset, load_image
import random


class Classifier_v1:
    """
    Agent for image classification. CNN with max pooling.
    """
    MODELPATH = join("models", "classifier_v1.keras")
    EPOCHS = 30
    TEST_SIZE = 0.3

    def __init__(self, train_if_new=True):
        """
        Try to find a saved model. Create a new one if not found.
        """

        if exists(self.MODELPATH):
            self.model = keras.models.load_model(self.MODELPATH)
            print("Loaded " + self.MODELPATH)
        else:
            self.model = self.create_model()
            if train_if_new:
                self.train()

    def create_model(self):
        """
        Create a convolutional neural network model.
        """
        model = Sequential()

        model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(IMG_WIDTH, IMG_HEIGHT, CHANNELS)))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(32, kernel_size=(3, 3)))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Flatten())
        model.add(Dense(128, activation='relu'))
        model.add(Dropout(0.3))

        model.add(Dense(NUM_CATEGORIES, activation='softmax'))
        model.compile(optimizer='adam', loss='categorical_crossentropy',
                      metrics=['accuracy'])
        return model

    def train(self):
        """
        Trains and saves model
        """
        images, labels = load_dataset()
        labels = keras.utils.to_categorical(labels)
        x_train, x_test, y_train, y_test = train_test_split(
            np.array(images), np.array(labels), test_size=self.TEST_SIZE
        )

        es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=5)

        save_best = keras.callbacks.ModelCheckpoint(self.MODELPATH, monitor='val_loss',
                                                    save_best_only=True, mode='min')

        datagen = ImageDataGenerator(rotation_range=30, zoom_range=0.10, fill_mode="nearest")
        self.model.fit_generator(datagen.flow(x_train, y_train), validation_data=(x_test, y_test),
                                 epochs=self.EPOCHS, callbacks=[es, save_best])

    def plot_prediction(self, image):
        """
        Load csv file at path and return determined class
        """
        result = self.model.predict_proba([[image]])
        class_index = np.argmax(result)

        f = plt.figure()
        ax = f.add_subplot(111)
        plt.imshow(image[..., 0], cmap="gray")
        plt.axis("off")
        plt.text(0.5, -0.05, "Class: " + str(class_index), horizontalalignment='center',
                 verticalalignment='center', transform=ax.transAxes)
        plt.show()

    def predict(self, image):
        return self.model.predict_proba([image])


if __name__ == "__main__":
    agent = Classifier_v1()
    # Change this to test other classes or leave empty to test all
    folder = "1triangle"
    images, labels = load_dataset(join("Samples", folder))
    random.shuffle(images)
    for img in images:
        agent.plot_prediction(img)