import argparse
from pythonosc import dispatcher
from pythonosc import osc_server
from pythonosc import udp_client
from agent import Agent
from os.path import join

"""
Sets up an osc server and waits for an osc message
containing the path to a csv file to classify. Sends
the result as an osc message to the set ip/port.
"""
class OscServer:
    def __init__(self):
        self.agent = Agent(False, False)

        parser = argparse.ArgumentParser()
        parser.add_argument("--ip", default="127.0.0.1",
                            help="The ip of the OSC server")
        parser.add_argument("--port", type=int, default=8001,
                            help="The port the other OSC server is listening on")
        args = parser.parse_args()
        self.client = udp_client.SimpleUDPClient(args.ip, args.port)
        parser = argparse.ArgumentParser()
        parser.add_argument("--ip",
                            default="127.0.0.1", help="The ip to listen on")
        parser.add_argument("--port",
                            type=int, default=8000, help="The port to listen on")
        args = parser.parse_args()

        self.dispatcher = dispatcher.Dispatcher()
        self.dispatcher.map("/request", self.on_message)
        self.server = osc_server.ThreadingOSCUDPServer(
            (args.ip, args.port), self.dispatcher)

    def run(self):
        print("Serving on {}".format(self.server.server_address))
        self.server.serve_forever()

    def on_message(self, addr, args):
        print("Received message")
        # args = filepath
        index = self.agent.predict(args)
        self.client.send_message("/result", int(index))


if __name__ == "__main__":
    server = OscServer()
    # Cudnn fails to initialize sometimes, so test if it's working properly
    server.on_message("/request", join("test_images", "test01.csv"))
    server.run()
