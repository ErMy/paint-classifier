from os.path import join
from os import walk
from autoencoder_v1 import Autoencoder_v1
from autoencoder_v2 import Autoencoder_v2


class AnomalyDetector:
    """
    Class for managing the different autoencoders. Creates two autoencoders (v1 and v2)
    for anomaly detection purposes for each symbol.
    """
    MODELPATH = "models"

    def __init__(self, version_1):
        # List of autoencoders
        self.autoencoders = []
        i = 0
        self.version1 = version_1

        # Go through each folder in /Samples and create an autoencoder based on the data inside each folder
        for r, d, f in walk("Samples"):
            for folder in d:
                # Don't create an autoencoder for the anomalies folder
                if "anomalies" in folder:
                    self.autoencoders.append(None)
                else:
                    if self.version1:
                        autoencoder = Autoencoder_v1(model_path=join(
                            self.MODELPATH, "autoencoder_v1_" + str(i) + ".keras"), data_path=join(r, folder))
                        self.autoencoders.append(autoencoder)
                    else:
                        autoencoder = Autoencoder_v2(model_path=join(
                            self.MODELPATH, "autoencoder_v2_" + str(i) + ".keras"), data_path=join(r, folder))
                        self.autoencoders.append(autoencoder)
                i += 1

    def train(self):
        if self.version1:
            for i in range(1, len(self.autoencoders)):
                self.autoencoders[i].train(save_path=join(self.MODELPATH, "autoencoder_v1_" + str(i) + ".keras"))
        else:
            for i in range(1, len(self.autoencoders)):
                self.autoencoders[i].train(save_path=join(self.MODELPATH, "autoencoder_v2_" + str(i) + ".keras"))

    def is_anomaly(self, class_index, image):
        """
        :param class_index: class index determined by the classifier
        :param image: numpy array
        :param v1: Use autoencoder_v1 or v2
        :return: True if anomaly
        """
        return self.autoencoders[class_index].is_anomaly(image)


if __name__ == "__main__":
    detector = AnomalyDetector()
