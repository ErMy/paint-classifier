from tensorflow import keras
from tensorflow.keras.layers import Conv2D, MaxPooling2D, UpSampling2D
from tensorflow.keras.models import Sequential
from os.path import join, exists
import numpy as np
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from utility import IMG_HEIGHT, IMG_WIDTH, CHANNELS, load_dataset
import random


class Autoencoder_v1:
    """
    Agent for anomaly detection using adadelta optimizer and a different architecture.
    Determine if an image contains a valid symbol.
    """
    def __init__(self, model_path, data_path, train_if_new=True):
        self.data_path = data_path
        if exists(model_path):
            self.model = keras.models.load_model(model_path)
            print("Loaded " + model_path)
        else:
            self.model = self.create_model()
            if train_if_new:
                self.train(save_path=model_path)

    def create_model(self):
        """
        Create a convolutional autoencoder model
        """
        model = Sequential()

        model.add(Conv2D(32, (3, 3), padding='same', activation='relu', input_shape=(IMG_WIDTH, IMG_HEIGHT, CHANNELS)))
        model.add(MaxPooling2D(pool_size=(2, 2),
                               padding='same'))  # using pool_size (4,4) makes the layer 4x smaller in height and width

        model.add(Conv2D(16, (3, 3), activation='relu', padding='same'))
        model.add(MaxPooling2D(pool_size=(2, 2), padding='same'))

        model.add(Conv2D(8, (3, 3), activation='relu', padding='same'))
        model.add(MaxPooling2D(pool_size=(2, 2), padding='same'))

        # -------------------------

        model.add(Conv2D(8, (3, 3), activation='relu', padding='same'))
        model.add(UpSampling2D((2, 2)))

        model.add(Conv2D(16, (3, 3), activation='relu', padding='same'))
        model.add(UpSampling2D((2, 2)))

        model.add(Conv2D(32, (3, 3), activation='relu', padding='same'))
        model.add(UpSampling2D((2, 2)))

        model.add(Conv2D(1, (3, 3), activation='sigmoid', padding='same'))
        # -------------------------

        model.compile(optimizer='adadelta', loss='binary_crossentropy')
        return model

    def train(self, save_path):
        images, labels = load_dataset(self.data_path)
        x_train, x_test, y_train, y_test = train_test_split(
            np.array(images), np.array(images), test_size=0.2
        )
        # Stop training if no improvement
        early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=30)

        # Save best version
        save_best = keras.callbacks.ModelCheckpoint(save_path, monitor='val_loss', save_best_only=True, mode='min')

        self.model.fit(x_train, y_train,
                       epochs=2000,
                       batch_size=32,
                       shuffle=True,
                       validation_data=(x_test, y_test),
                       callbacks=[early_stop, save_best])

    def is_anomaly(self, image):
        """
        Reconstructs image and compares to original.
        Considered anomaly if error above threshold.
        :return: True if anomaly
        """
        threshold = 0.9
        reconstruction_error = self.model.evaluate([[image]], [[image]], verbose=0)
        reconstruction = self.model.predict([[image]])
        if reconstruction_error > threshold:
            return True, reconstruction, reconstruction_error
        else:
            return False, reconstruction, reconstruction_error

    def plot_prediction(self, image):
        """
        Plot original image and its reconstruction
        """
        reconstruction = self.model.predict([[image]])
        reconstruction_error = self.model.evaluate([[image]], [[image]], verbose=0)
        plt.subplot(1, 2, 1)
        plt.imshow(image[..., 0], cmap="gray")
        plt.axis("off")
        ax = plt.subplot(1, 2, 2)
        plt.imshow(reconstruction[0, ..., 0], cmap="gray")
        plt.text(0.5, -0.05, "Error: " + str(reconstruction_error), horizontalalignment='center',
                 verticalalignment='center', transform=ax.transAxes)
        plt.axis("off")
        plt.show()


if __name__ == "__main__":
    # Change name to use other model (_1 is for autoencoder trained on symbol 1, _2 for 2 etc)
    filename = "autoencoder_v1_2.keras"
    autoencoder = Autoencoder_v1(model_path=join("models", filename),
                                 data_path=join("Samples", "1triangle"), train_if_new=False)

    # Change this to use different data or leave empty to use all data
    folder = "2clock"
    images, labels = load_dataset(join("Samples", folder))
    random.shuffle(images)
    # Plot original vs reconstructions
    for img in images:
        autoencoder.plot_prediction(img)
