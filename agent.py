from classifier_v1 import Classifier_v1
from classifier_v2 import Classifier_v2
from anomalydetector import AnomalyDetector
import numpy as np
from utility import load_image
from os.path import join, isfile
from os import listdir
import matplotlib.pyplot as plt

"""
Main class providing access to the trained agents. Agents are automatically trained if saved models aren't found.
"""
class Agent:
    def __init__(self, class_v1=False, auto_v1=False):
        if class_v1:
            self.classifier = Classifier_v1()
        else:
            self.classifier = Classifier_v2()

        self.anomaly_detector = AnomalyDetector(auto_v1)

    def predict(self, file_path):
        """
        Determines class of given image.
        First, classify image. Then check if it's an anomaly.
        Then classify again with the reconstructed image
        from the autoencoder.
        :param file_path: Path to csv image file
        :param class_v1: Use Classifier_v1 or v2
        :param auto_v1: Use Autoencoder_v1 or v2
        :return Predicted class index. -1 if not recognized.
        """
        image = load_image(file_path)
        probability = self.classifier.predict([image])
        class_index = np.argmax(probability)

        print("Probability", probability)

        if np.max(probability) < 0.9 or class_index == 0:
            return 0

        is_anomaly, reconstruction, error = self.anomaly_detector.is_anomaly(class_index, image)
        print("Anomaly:", is_anomaly)
        if is_anomaly:
            return 0
        else:
            probability = self.classifier.predict(reconstruction)
            class_index2 = np.argmax(probability)
            if np.max(probability) < 0.9 or class_index != class_index2:
                return 0
            else:
                return class_index

    def train_classifier(self):
        self.classifier.train()

    def train_anomaly_detector(self):
        self.anomaly_detector.train()

    def test(self):
        """
        Loads test data and plots images with their predictions
        """
        path = "test_images"
        all_files = [join(path, f) for f in listdir(path) if isfile(join(path, f))]
        images = []
        classes = []
        confidence = []
        reconstructions = []
        anomalies = []
        rec_error = []
        rec_classes = []
        rec_confidence = []
        for file in all_files:
            images.append(load_image(file))

        for img in images:
            prob = self.classifier.predict([img])
            index = np.argmax(prob)
            classes.append(index)
            confidence.append(np.max(prob))
            # Index 0 == anomaly, -> no reconstruction
            if index == 0:
                anomalies.append(True)
                reconstructions.append(None)
                rec_error.append("Not available")
                rec_classes.append("Not available")
                rec_confidence.append("Not available")
            else:
                anomaly, rec, error = self.anomaly_detector.is_anomaly(index, img)
                anomalies.append(anomaly)
                reconstructions.append(rec)
                prob = self.classifier.predict(rec)
                index = np.argmax(prob)
                rec_error.append(error)
                rec_classes.append(index)
                rec_confidence.append(np.max(prob))

        # Plot images
        x = len(images)
        for i in range(x):
            f = plt.figure()
            ax = f.add_subplot(1, 2, 1)
            plt.imshow(images[i][..., 0], cmap="gray")
            text = "Class: " + str(classes[i]) + "\nConfidence: " + str(confidence[i]) + "\nAnomaly: " + str(anomalies[i])
            plt.text(0.5, -0.05, text, horizontalalignment='center',
                     verticalalignment='top', transform=ax.transAxes)
            plt.axis("off")

            ax = f.add_subplot(1, 2, 2)
            if reconstructions[i] is None:
                plt.imshow(images[i][..., 0], cmap="gray")
            else:
                plt.imshow(reconstructions[i][0, ..., 0], cmap="gray")
            text = "Error: " + str(rec_error[i]) + "\nClass: " + str(rec_classes[i]) + "\nConfidence: " + str(rec_confidence[i])
            plt.text(0.5, -0.05, text, horizontalalignment='center',
                     verticalalignment='top', transform=ax.transAxes)
            plt.axis("off")
        plt.show()


if __name__ == "__main__":
    agent = Agent(False, False)
    # agent.train_classifier()
    # agent.train_anomaly_detector()
    agent.test()
